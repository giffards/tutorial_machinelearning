This is a simple tutorial to get used to machine learning using scikit-learn, with a geoscience example. 

_Vea a continuación las versiones en español de las instrucciones_

**Requirements**
******************

Python >3 with:
- numpy
- matplotlib
- pandas
- scikit-learn (sklearn)


********************
**How to get started:**
********************

First, clone or download the repository from the icons on the up right.

Then, two options:

**A) preferred - use Conda and Jupyter on your computer.**

_Linux:_

1. install conda (for ex. using Miniconda):
        https://docs.conda.io/en/latest/miniconda.html , download the installer link and follow the installation instructions https://conda.io/projects/conda/en/latest/user-guide/install/index.html
2. create a new environment with conda and with all the libraries (run on a terminal:):
        `conda create -n tutorial python=3 scikit-learn numpy pandas matplotlib`
3. activate this new env.: 
        `conda activate tutorial`
4. test it, by launching:
        `python` (then type `exit` to close it)
5. Then, you can use jupyter notebook 
        `cd Tutorial_machinelearning/`
        `conda install jupyter `
        `jupyter notebook `
6. on your browser, open the ML_tofill.ipynb
     
_Windows:_
     
1. install Anaconda.
2. open environment --> create (with a Python version >3)
3. add the 4 libraires and jupyter ( numpy, matplotlib, pandas, scikit-learn, jupyter ) either from the Anaconda terminal by typing then `conda install name_libraries` or by adding them manually.
4. Then, you can use jupyter notebook, for example by typing in the Anaconda terminal:
    `jupyter notebook` 
5. on your browser, open the ML_tofill.ipynb
        
**B) use Colab:** https://colab.research.google.com

1. create an account
2. go to: file --> upload notebook (choose the 'ML_tofill.ipynb')
3. click on the 'Files' logo on the left, then to the first icon 'file upload' and add the data/train_data_Nepal_damage.csv and data/test_data_Nepal_damage.csv
4. start the notebook
    
    
**************************************
**ESPANOL**
**************************************

Este es un sencillo tutorial para acostumbrarse al aprendizaje automático con scikit-learn, con un ejemplo de geociencia. 

**Requisitos**
******************

Python >3 con:
- numpy
- matplotlib
- pandas
- scikit-learn (sklearn)


********************
**Cómo empezar:**
********************

Primero, clona o descarga el repositorio desde los iconos de la parte superior derecha.

Luego, dos opciones:

**A) la preferida: utilizar Conda y Jupyter en tu ordenador.**

_Linux:_

     
1. instalar Conda (por ejemplo, usando Miniconda):
         https://docs.conda.io/en/latest/miniconda.html , descarga el enlace del instalador y sigue las instrucciones de instalación https://conda.io/projects/conda/en/latest/user-guide/install/index.html
2.  crear un nuevo entorno con conda y con todas las librerías (type on a terminal:):
         `conda create -n tutorial python=3 scikit-learn numpy pandas matplotlib`
3. activar este nuevo entorno.: 
         `conda activate tutorial`
4. pruébalo, lanzando:
         `python` (luego escribe `exit` para cerrarlo)
5. Luego, puedes usar el jupyter notebook
        `cd Tutorial_machinelearning/`
        `conda install jupyter` 
        `jupyter notebook` 
6. en su navegador, abra el ML_tofill.ipynb
     
_Windows:_
     
1. instalar Anaconda.
2. abrir el entorno --> crear (con una versión de Python >3)
3. añadir las 4 librerías y jupyter ( numpy, matplotlib, pandas, scikit-learn, jupyter ) bien desde el terminal de Anaconda escribiendo entonces `conda install name_library` o añadiéndolas manualmente.
4. Luego, puedes usar jupyter notebook, por ejemplo escribiendo en la terminal de Anaconda
        `jupyter notebook` 
5. en su navegador, abra el ML_tofill.ipynb
        
**B) utilice Colab:** https://colab.research.google.com

1. crear una cuenta
2. ir a: archivo (file) --> upload notebook (elegir el 'ML_tofill.ipynb')
3. haga clic en el logotipo de "Archivos" ('Files') a la izquierda, luego en el primer icono "carga de archivos" (file upload) y añada los data/train_data_Nepal_damage.csv y data/test_data_Nepal_damage.csv
4. iniciar el notebook
